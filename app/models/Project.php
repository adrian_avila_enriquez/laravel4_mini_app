<?php 


	/*
	 * Every model extend class Eloquent
	 * Eloquent ORM is an elegant object which enables simple interaction with DB
	 * Evert table in DB is represented with a Model.
	 * All models have 3 basic attributes : id, created_at, updated_at
	*/

	class Project extends Eloquent { 
    	protected $table = 'projects';

    	protected $fillable = array('pname', 'start_month', 'end_month');

    	// Relation MANY TO MANY
    	public function normalusers(){

        	return $this->belongsToMany('NormalUser', 'worksIn', 'user_id', 'project_id')
        								->withPivot('hours_worked');

    	}

	}
?>