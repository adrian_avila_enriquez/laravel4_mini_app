<?php 
	/*
	 * Every model extend class Eloquent
	 * Eloquent ORM is an elegant object which enables simple interaction with DB
	 * Evert table in DB is represented with a Model.
	 * All models have 3 basic attributes : id, created_at, updated_at
	*/

	class NormalUser extends Eloquent { 
    	protected $table = 'normal_users';

    	protected $fillable = array('username', 'email', 'password');

    	// Relation MANY TO MANY ( M : N )
    	public function projects(){

        	return $this->belongsToMany('Project', 'worksIn', 'user_id', 'project_id')
        								->withPivot('hours_worked');
    	}

	}
?>