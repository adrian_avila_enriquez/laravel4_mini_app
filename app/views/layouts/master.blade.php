<html>

	<body>
	@section('sidebar')
		<!--<a href="http://azurlingua.com">
			<img src="http://res.cloudinary.com/azurlingua/image/upload/v1424552175/logo-azur-lingua-bdf_exfenu.png">
		</a>-->
		<h1> Azurlingua :: Azurmedia Department </h1>
		{{ HTML::link('/', 'Home'); }} 
		@show

		<div class="container">
			
			@yield('content')

		</div>

		<!-- @section, @show, @yield : Blade functions for creating HTML templates (repetitive style)-->
	
	</body>

</html>
