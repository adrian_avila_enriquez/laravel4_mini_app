@extends('layouts.master')
 
@section('sidebar')
     @parent
     <h2> User Profile </h2>
@stop
 
@section('content')
<h3> Basic Information. </h3>
<ul>
    <li> Username : <h4>{{ $normaluser->username }}</h4> </li>
    <li> email : <h4>{{ $normaluser->email }}</h4></li>     			
</ul>
<h3> This user is currently working on project: </h3>
<ul>
	@foreach($projects as $project)
	<li> {{ HTML::link( 'projects/'.$project->pd , $project->pname ) }} </li>
	@endforeach 
</ul>
<h4> {{ HTML::link('/users', 'Back to Users'); }} </h4>       
@stop