@extends('layouts.master')


@section('sidebar')
     @parent
     <h3> Normal Users </h3>
@stop

@section('content')	
<ul>
  @foreach($normalusers as $normaluser)
  <!-- Blade equivalent to PHP: <?php //foreach ($usuarios as $usuario) ?> -->
    <li>
      N-User # {{$normaluser->id}} :: {{ HTML::link( 'users/'.$normaluser->id , $normaluser->username ) }}
      <!-- HTML Class allows to create: <a href=""></a> to specific view-->

      <!-- <h4> Current normal user: {{ $normaluser->username}}</h4> <h3>:: email: {{ $normaluser->email }} </h3> -->
    </li>
    <!-- Blade equivalent to PHP: <?php //echo $usuario->nombre.' '.$usuario->apellido ?> -->
  @endforeach 
</ul>

<h4> {{ HTML::link('users/new', 'Register new NormalUser'); }} </h4>
@stop