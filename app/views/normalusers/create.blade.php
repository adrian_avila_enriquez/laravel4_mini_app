@extends('layouts.master')
 
@section('sidebar')
     @parent
     <h2> Register New N-User </h2>
@stop
 
@section('content')
        {{ HTML::link('users', 'Go back'); }}

        {{ Form::open(array('url' => 'users/create')) }}<!-- <form> ,parameter: route to send data-->
                                                            <!-- action="azurmedia/create" -->
            {{Form::label('username', 'Username')}}         <!-- <label> -->
            {{Form::text('username', '')}}                  <!-- <input class="text" > -->
            {{Form::label('email', 'Email')}}               <!-- -->
            {{Form::text('email', '')}}                     <!-- -->
            {{Form::label('password', 'Password')}}         <!-- -->
            {{Form::password('password', '')}}                  <!-- -->
            {{Form::submit('Register !')}}                  <!-- <input class="submit"> -->
        {{ Form::close() }}                                 <!--</form> -->
@stop