@extends('layouts.master')


@section('sidebar')
     @parent
     <h3> Projects Catalog </h3>
@stop

@section('content')	
<ul>
  @foreach($projects as $project)
    <li>
      	Project # {{$project->id }}
      	:: {{$project->pname }}
      	:: {{HTML::link('projects/'.$project->id , 'See details');}}
    </li>
  @endforeach 
</ul>
@stop