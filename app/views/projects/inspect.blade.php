@extends('layouts.master')
 

@section('sidebar')
     @parent
     <h2> Project Information </h2>
@stop
 
@section('content')
<h3> General description. </h3>
<ul>
	<li> Project name : <h4>{{ $project->pname }}</h4></li>
	<li> Period : <h4>{{ $project->start_month.' - '.$project->end_month }}</h4> </li> 
</ul>

<h3> N-Users currently working on project. </h3>
<ul>
	@foreach($normalusers as $normaluser)
	<li> {{ HTML::link( 'users/'.$normaluser->id , $normaluser->username ) }} [Total work: {{$normaluser->pivot->hours_worked}} hrs] </li>
	@endforeach 
</ul>

<h4> {{ HTML::link('/projects/updateUsers', 'Update project information'); }} </h4>
<h4> {{ HTML::link('/projects', 'Back to Projects'); }} </h4>  
@stop