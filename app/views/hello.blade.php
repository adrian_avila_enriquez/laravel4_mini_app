<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Laravel PHP Framework</title>
    <style>
        @import url(http://fonts.googleapis.com/css?family=Lato:300,400,700);

        body {
            margin:0;
            font-family:'Lato', sans-serif;
            text-align:center;
            color: #999;
        }

        .welcome {
           width: 300px;
           height: 300px;
           position: absolute;
           left: 50%;
           top: 40%; 
           margin-left: -150px;
           margin-top: -150px;
        }

        a, a:visited {
            color:#FF5949;
            text-decoration:none;
        }

        a:hover {
            text-decoration:underline;
        }

        ul li {
            display:inline;
            margin:0 1.2em;
        }

        p {
            margin:2em 0;
            color:#555;
        }
    </style>
</head>
<body>
    <div class="welcome">
        <a href="http://azurlingua.com" title="Azurmedia Mini App"><img src="http://res.cloudinary.com/azurlingua/image/upload/v1424552175/logo-azur-lingua-bdf_exfenu.png"</a>    
    <div>
    <div>
        <h2>Welcome to Azurmedia</h2>
        <h4> {{ HTML::link('users', 'Department'); }} </h4>
        <h4> {{ HTML::link('projects', 'Projects'); }} </h4>      
    </div>    
</body>
</html>
