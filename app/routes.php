<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Home route
Route::get('/', function()
{
	return View::make('hello');
});

//Users routes
Route::get('users', array('uses' => 'NormalUsersController@showNormalUsers'));

Route::get('users/new', array('uses' => 'NormalUsersController@newNormalUser'));

Route::post('users/create', array('uses' => 'NormalUsersController@createNormalUser'));

// This route points to the FORM for creating the new normaluser (POST requests)
Route::get('users/{id}', array('uses'=>'NormalUsersController@searchNormalUser'));

// For including parametes, we use {parameter}, if parameter is optional synthaxis is {parameter?}


//Projects routes
Route::get('projects', array('uses' => 'ProjectController@showProjects'));

Route::get('projects/{pid}', array('uses'=>'ProjectController@inspectProject'));

Route::get('projects/{pid}/updateUsers', array('uses'=>'ProjectController@updateUsers'));



