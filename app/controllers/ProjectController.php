<?php 
class ProjectController extends BaseController {

   
    //Shows list with all existent normal users 
    public function showProjects()
    {
        $projects = Project::all(); 
        
        /*
         * all() method looks for every record inside specific table 
         * It returns the content of that table
         */
        
        return View::make('projects.catalog', array('projects' => $projects));

        /*
         * make() method of class View indicates which view we want to use
         * We send normalusers , an array with the data stored 
        */
    } 

    public function inspectProject($pid){

        $project = Project::find($pid);
        // $project will contain the data for the project record with id = $pid

        return View::make('projects.inspect', array('project' => $project, 'normalusers' => $project->normalusers));
        // We return the appropiate view for display the info of searched project
    }

    public function updateUsers(){

        echo 'Nothing yet';
    }



}
?>