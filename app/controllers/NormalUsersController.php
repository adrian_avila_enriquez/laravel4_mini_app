<?php 
class NormalUsersController extends BaseController {
    
    //Shows list with all existent normal users 
    public function showNormalUsers()
    {
        $normalusers = NormalUser::all(); 
        
        /*
         * all() method looks for every record inside specific table 
         * It returns the content of that table
         */
        
        return View::make('normalusers.list', array('normalusers' => $normalusers));

        /*
         * make() method of class View indicates which view we want to use
         * We send normalusers , an array with the data stored 
        */
    }

    //Shows FORM for the creation of new normaluser
    public function newNormalUser()
    {
        return View::make('normalusers.create');
        // Again, make() method indicates the correct View
    }
    
    //Creates the new normal user
    public function createNormalUser()
    {
        NormalUser::create(Input::all());

        /*
         * create() method allows to create a new normaluser in DB
         * receives an array with the Model-data and inserts this data into DB
         * in this case, data comes from the FORM for new normaluser ( using Input::all() )
         */

        // Input::all() getting all input for the request
 
        return Redirect::to('users');
        // This function will redirect us to the route 'azurmedia' (for showing all normalusers)
 
    }

    // Search normaluser using its id
    public function searchNormalUser($id)
    {   
        $normaluser = NormalUser::find($id);
        // find() method return an object with all attributes contained in a normaluser with id = $id
         
    
        return View::make('normalusers.search', array('normaluser' => $normaluser, 'projects' => $normaluser->projects));
        // We return the appropiate view for display the info of searched normaluser
    }
    
}
?>