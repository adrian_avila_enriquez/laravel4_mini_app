CREATE DATABASE 'laravel4_DB';

use laravel4_DB;

CREATE TABLE `normal_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
);


INSERT INTO normal_users values('1','adrian', 'azurmedia.ad@gmail.com', 'avila', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO normal_users values('2','ludovic', 'azurmedia.lu@gmail.com', 'floch', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO normal_users values('3','yann', 'azurlingua@gmail.com', 'librati', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO normal_users values('4','patricia', 'azurmedia.pa@gmail.com', 'camacho', '0000-00-00 00:00:00', '0000-00-00 00:00:00');