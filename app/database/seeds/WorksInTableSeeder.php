<?php

class WorksInTableSeeder extends Seeder{

	public function run()
	{
		\DB::table('worksIn')->insert(array(
												array( 
													'user_id' => '1',
													'project_id' => '1',
													'hours_worked' => '10'
												),
												array( 
													'user_id' => '2',
													'project_id' => '2',
													'hours_worked' => '20'
												),
												array( 
													'user_id' => '3',
													'project_id' => '3',
													'hours_worked' => '30'
												),
												array( 
													'user_id' => '4',
													'project_id' => '4',
													'hours_worked' => '40'
												)												
											)
										);

	}

}