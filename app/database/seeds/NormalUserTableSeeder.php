<?php

class NormalUserTableSeeder extends Seeder{

	public function run()
	{
		\DB::table('normal_users')->insert(array(
												array( 
													'username' => 'adrian',
													'email' => 'azurmedia.ad@gmail.com',
													'password' => \Hash::make('avila')
												),
												array( 
													'username' => 'ludovic',
													'email' => 'azurmedia.lu@gmail.com',
													'password' => \Hash::make('floch')
												),
												array( 
													'username' => 'yann',
													'email' => 'azurlingua@gmail.com',
													'password' => \Hash::make('librati')
												),
												array( 
													'username' => 'patricia',
													'email' => 'azurmedia.pa@gmail.com',
													'password' => \Hash::make('camacho')
												)
											)
										);

	}

}