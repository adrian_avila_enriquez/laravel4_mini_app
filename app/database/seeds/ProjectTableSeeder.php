<?php

class ProjectTableSeeder extends Seeder{

	public function run()
	{
		\DB::table('projects')->insert(array(
											array( 
													'pname' => 'Francophony',
													'start_month' => 'January',
													'end_month' => 'December'
											),
											array( 
													'pname' => 'Azur_Cloud.2',
													'start_month' => 'January',
													'end_month' => 'December'
											),
											array( 
													'pname' => 'Videogame10',
													'start_month' => 'January',
													'end_month' => 'December'
											),
											array( 
													'pname' => 'SecretProject',
													'start_month' => 'January',
													'end_month' => 'December'
											)
										)
									);

	}

}