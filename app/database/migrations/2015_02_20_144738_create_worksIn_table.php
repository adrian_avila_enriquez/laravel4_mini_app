<?php

use Illuminate\Database\Migrations\Migration;

class CreateWorksInTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('worksIn', function($table)
        {
        	$table->engine = 'InnoDB';
            $table->unsignedInteger('user_id');          
            $table->unsignedInteger('project_id');
            $table->primary(array('user_id', 'project_id'));
            $table->foreign('user_id')
							->references('id')->on('normal_users')
							->onUpdate('cascade')
							->onDelete('cascade');
            $table->foreign('project_id')
							->references('id')->on('projects')
							->onUpdate('cascade')
							->onDelete('cascade');
            $table->integer('hours_worked');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('worksIn');
	}

}