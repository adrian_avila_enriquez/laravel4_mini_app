<?php

use Illuminate\Database\Migrations\Migration;

class CreateNormalUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('normal_users', function($table)
        {
            $table->increments('id')->unsigned();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password')->unique();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('normal_users');
	}

}